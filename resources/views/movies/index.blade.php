@extends('layouts.app')

@section('content')

    <div class="col-md-6 text-end">
        <a href="{{ route('movies.create') }}" class="btn btn-primary">Create new</a>
        @if (session('message'))
            <h3 style="color:green">{{ session('message') }}</h3>
        @endif
    </div>

    <div class="container table-responsive py-5">
        <table class="table table-bordered table-hover">
            <thead class="thead-dark">
            <tr>
                <th>Title</th>
                <th>Year</th>
                <th>Genre</th>
                <th>Description</th>
                <th>Active</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($movies))
                @foreach($movies as $movie)
                    <tr>
                        <td>{{ $movie->title }}</td>
                        <td>{{ $movie->year }}</td>
                        <td>{{ $movie->genre }}</td>
                        <td>{{ $movie->description }}</td>
                        <td>{{ $movie->active ? 'Active' : 'Inactive' }}</td>
                        <td>
                            <div class="btn-group">
                                <a href="{{ route('movies.edit', $movie->id) }}" class="btn btn-primary ">Edit</a>

                                <form action="{{ route('movies.destroy', $movie->id) }}" method="post"
                                      class="">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@endsection

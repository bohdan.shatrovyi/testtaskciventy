@extends('layouts.app')

@section('content')


    <div class="container p-3">
        <div class="row d-flex justify-content-center">

            <div class="col-md-6 p-3 border">

                <h2>Create movie</h2>

                <form action="{{ route('movies.store') }}" method="post">
                    @method('POST')
                    @csrf

                    <div class="mb-3">
                        <label for="title" class="form-label">Title</label>
                        <input type="text" name="title" id="title" class="form-control">
                    </div>

                    <div class="mb-3">
                        <label for="year" class="form-label">Year</label>
                        <input type="number" id="year" name="year" min="1900" max="2099" placeholder="YYYY">
                    </div>
                    <div class="mb-3">
                        <label for="genre" class="form-label">Genre</label>
                        <input type="text" name="genre" id="genre" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">Description</label>
                        <input type="text" name="description" id="description" class="form-control">
                    </div>
                    <div class="mb-3">
                        <label for="status" class="form-label">active</label>
                        <select name="active" id="active" class="form-select">
                            <option value="0">Non active</option>
                            <option value="1">Active</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>

@endsection

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        $userData = $request->only('email', 'password');

        if (Auth::attempt($userData)) {
            return redirect()->intended('/movies');
        } else {
            return redirect()->back()->withErrors([
                'email' => 'Неправильний email або пароль',
            ]);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

     public function showRegistrationForm()
    {
        return view('auth.register');
    }

    // Реєстрація нового користувача
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        Auth::login($user);

        return redirect('/movies');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index(Request $request)
    {
        $request->validate([
            'title' => 'string|max:255',
            'genre' => 'string|max:255',
            'year' => 'integer',
            'year_from' => 'integer',
            'year_to' => 'integer',
            'description' => 'string|max:255',
        ]);

        $movies = Movie::query();

        if ($request->has('title')) {
            $movies->where('title', 'like', '%' . $request->input('title') . '%');
        }

        if ($request->has('genre')) {
            $movies->where('genre', 'like', '%' . $request->input('genre') . '%');
        }

        if ($request->has('year')) {
            $movies->where('year', $request->input('year'));
        }

        if ($request->has('year_from') && $request->has('year_to')) {
            $movies->whereBetween('year', [$request->input('year_from'), $request->input('year_to')]);
        }

        if ($request->has('description')) {
            $movies->where('description', 'like', '%' . $request->input('description') . '%');
        }

        $movies = $movies->paginate(10);

        return response()->json($movies);
    }
}

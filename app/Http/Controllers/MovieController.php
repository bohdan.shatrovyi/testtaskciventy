<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    public function index()
    {
        $movies = Movie::all();
        return view('movies.index', compact('movies'));
    }

    public function create()
    {
        return view('movies.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|max:255',
            'genre' => 'string|max:255',
            'year' => 'integer',
            'description' => 'string|max:255',
        ]);

        $movie = new Movie();

        $movie->title =  $request->input('title');
        $movie->year =  $request->input('year');
        $movie->genre =  $request->input('genre');
        $movie->description =  $request->input('description');
        $movie->active =  $request->input('active');

        $movie->save();

        return redirect()->route('movies.index')->with('message', 'Movie added successfully');
    }

    public function edit(string $id)
    {
        $movie = Movie::find($id);
        return view('movies.edit', compact('movie'));
    }

    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'string|max:255',
            'genre' => 'string|max:255',
            'year' => 'integer',
            'description' => 'string|max:255',
        ]);

        $movie = Movie::find($id);

        $movie->title =  $request->input('title');
        $movie->year =  $request->input('year');
        $movie->genre =  $request->input('genre');
        $movie->description =  $request->input('description');
        $movie->active =  $request->input('active');

        $movie->save();

        return redirect()->route('movies.index')->with('message', "movie updated successfully");
    }

    public function destroy(string $id)
    {
        $movie = Movie::find($id);
        $movie->delete();

        return redirect()->route('movies.index')->with('message', 'Movie deleted successfully');
    }
}
